/**
 * 
 */
package lambda;

/**
 * @author boris
 *
 */
@FunctionalInterface
public interface MyName {
	public String myName(String name);
}
