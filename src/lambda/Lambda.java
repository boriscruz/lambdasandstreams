package lambda;

public class Lambda implements ByDefault{

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		MyName myAnonymousName = new MyName() {
			@Override
			public String myName(String name) {
				return name;
			}
		};

		System.out.println(myAnonymousName.myName("Roo without lambda"));

		MyName myFirstLambda = (name) -> name;

		System.out.println(myFirstLambda.myName("Roo with lambda"));

		Sum sum = (a, b) -> a + b;

		System.out.println(sum.getSum(3, 12));
		
		Lambda lambda = new Lambda();
		
		System.out.println(lambda.nameByDefault("Roo"));

	}

	@Override
	public void showName(String name) {
		// TODO Auto-generated method stub
		
	}

}
