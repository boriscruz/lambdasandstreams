/**
 * 
 */
package lambda;

/**
 * @author boris
 *
 */
@FunctionalInterface
public interface ByDefault {
	
	void showName(String name);

	default String nameByDefault(String name) {
		return name + " Default";
	}
}
