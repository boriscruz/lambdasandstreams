/**
 * 
 */
package lambda;

/**
 * @author boris
 *
 */
@FunctionalInterface
public interface Sum {
	int getSum(int a, int b);
}
