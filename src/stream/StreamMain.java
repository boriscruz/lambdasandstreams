/**
 * 
 */
package stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author boris
 *
 */
public class StreamMain {

	public static List<User> users;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("----------------------Streams----------------------");
		setUpUser();
		users.stream().forEach(user -> user.setName(user.getName() + " Bel"));
		printList();
		System.out.println("----------------------map----------------------");
		List<String> namesList = users.stream().map(user -> user.getName()).collect(Collectors.toList());
		namesList.stream().forEach(element -> System.out.println(element));
		System.out.println("----------------------filter----------------------");
		setUpUser();
		List<User> usersFilter = users.stream().filter(user -> !user.getName().equals("Roo 1"))
				.filter(user -> user.getId() > 1).collect(Collectors.toList());
		usersFilter.stream().forEach(user -> System.out.println(user));

		System.out.println("----------------------find first----------------------");
		User foundUser = users.stream().filter(user -> user.getName().equals("Roo 1")).findFirst()
				.orElseThrow(NoSuchElementException::new);
		System.out.println(foundUser);

		System.out.println("----------------------flat map----------------------");
		List<List<String>> manyLists = new ArrayList<List<String>>(
				Arrays.asList(new ArrayList<String>(Arrays.asList("Roo 1", "Bel 1", "Mic 1")),
						new ArrayList<String>(Arrays.asList("Roo 2", "Bel 2", "Mic 2", "Mic 2")),
						new ArrayList<String>(Arrays.asList("Roo 3", "Bel 3", "Mic 3", "Mic 3", "Mic 3"))));

		List<String> oneList = manyLists.stream().flatMap(list -> list.stream()).collect(Collectors.toList());

		oneList.forEach(element -> System.out.println(element));

		System.out.println("----------------------peek----------------------");
		setUpUser();
		List<User> usersList = users.stream().peek(user -> user.setName(user.getName() + " Bel"))
				.collect(Collectors.toList());

		usersList.forEach(user -> System.out.println(user.toString()));

		System.out.println("----------------------count----------------------");
		long totalFilter = users.stream().filter(user -> !user.getName().equals("Roo 1")).count();
		System.out.println(totalFilter);

		System.out.println("----------------------skip and limit----------------------");
		String[] abc = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j" };
		List<String> abcFilter = Arrays.stream(abc).skip(3).limit(5).collect(Collectors.toList());
		abcFilter.forEach(element -> System.out.println(element));

		System.out.println("----------------------Sorted----------------------");
		setUpUser();
		users = users.stream().sorted(Comparator.comparing(User::getId)).collect(Collectors.toList());
		printList();

		System.out.println("----------------------min y max----------------------");
		setUpUser();
		User userMin = users.stream().min(Comparator.comparing(User::getId)).orElse(null);
		User userMax = users.stream().max(Comparator.comparing(User::getId)).orElse(null);

		System.out.println(userMin);
		System.out.println(userMax);

		System.out.println("----------------------distinc----------------------");
		String[] abc1 = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "c", "f" };
		List<String> abc1Filter = Arrays.stream(abc1).distinct().collect(Collectors.toList());
		abc1Filter.stream().forEach(element -> System.out.println(element));

		System.out.println("----------------------allMatch, anyMatch, noneMatch----------------------");

		List<Integer> numberList = Arrays.asList(100, 200, 300, 50, 1000, 5000);

		boolean allMatch = numberList.stream().allMatch(number -> number > 310);
		System.out.println(allMatch);

		boolean anyMatch = numberList.stream().anyMatch(number -> number > 310);
		System.out.println(anyMatch);

		boolean noneMach = numberList.stream().noneMatch(number -> number < 10000);
		System.out.println(noneMach);

		System.out.println("----------------------Sum, average, range----------------------");
		setUpUser();

		double result = users.stream().mapToInt(User::getId).average().orElse(0);
		System.out.println(result);

		result = users.stream().mapToInt(User::getId).sum();
		System.out.println(result);

		System.out.println(IntStream.range(0, 100).sum());

		System.out.println("----------------------Reduce----------------------");
		setUpUser();
		int number = users.stream().map(User::getId).reduce(0, Integer::sum);
		System.out.println(number);

		System.out.println("----------------------joining----------------------");
		setUpUser();
		String names = users.stream().map(User::getName).collect(Collectors.joining(" - ", "(", ")"));
		System.out.println(names);

		System.out.println("----------------------toSet----------------------");
		setUpUser();
		Set<String> setNames = users.stream().map(User::getName).collect(Collectors.toSet());
		setNames.stream().forEach(name -> System.out.println(name));

		System.out.println("----------------------summarizingDouble----------------------");
		DoubleSummaryStatistics statitics = users.stream().collect(Collectors.summarizingDouble(User::getId));
		System.out.println("media: " + statitics.getAverage() + "maximo: " + statitics.getMax() + "minimo: "
				+ statitics.getMin() + "cantidad: " + statitics.getCount() + "total : " + statitics.getSum());
		DoubleSummaryStatistics statitics1 = users.stream().mapToDouble(User::getId).summaryStatistics();
		System.out.println("media: " + statitics1.getAverage() + "maximo: " + statitics1.getMax() + "minimo: "
				+ statitics1.getMin() + "cantidad: " + statitics1.getCount() + "total : " + statitics1.getSum());

		System.out.println("----------------------partitioningBy----------------------");
		List<Integer> numbers = Arrays.asList(5, 7, 42, 32, 3, 42, 2, 6, 4, 23, 7, 54, 65, 3);
		Map<Boolean, List<Integer>> mayoresADiez = numbers.stream().collect(Collectors.partitioningBy(e -> e > 10));
		System.out.println("Menores a 10: ");
		mayoresADiez.get(false).stream().forEach(e -> System.out.println(e));
		System.out.println("Mayores a 10: ");
		mayoresADiez.get(true).stream().forEach(e -> System.out.println(e));
		
		System.out.println("----------------------groupingBy----------------------");
		
		setUpUser();
		
		Map<Integer, List<User>> idsGroup = users.stream().collect(Collectors.groupingBy(e -> e.getId()));
		System.out.println("usuarios con id  = 1");
		idsGroup.get(1).stream().forEach(user->System.out.println(user));
		System.out.println("usuarios con id  = 2");
		idsGroup.get(2).stream().forEach(user->System.out.println(user));
		System.out.println("usuarios con id  = 3");
		idsGroup.get(3).stream().forEach(user->System.out.println(user));
		System.out.println("usuarios con id  = 4");
		idsGroup.get(4).stream().forEach(user->System.out.println(user));
		System.out.println("usuarios con id  = 5");
		idsGroup.get(5).stream().forEach(user->System.out.println(user));

	}

	private static void setUpUser() {
		users = new ArrayList<User>();
		users.add(new User(1, "Roo 1"));
		users.add(new User(2, "Roo 2"));
		users.add(new User(3, "Roo 1"));
		users.add(new User(4, "Roo 4"));
		users.add(new User(5, "Roo 1"));
		users.add(new User(4, "Roo 4"));
		users.add(new User(4, "Roo 4"));

	}

	private static void printList() {
		users.stream().forEach(user -> System.out.println(user.toString()));
	}

}
