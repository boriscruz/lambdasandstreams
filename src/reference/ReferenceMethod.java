/**
 * 
 */
package reference;

import java.util.ArrayList;
import java.util.List;

/**
 * @author boris
 *
 */
public class ReferenceMethod {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		/* Referencia a metodo estatico */
		Job job = new Job() {
			@Override
			public void action() {
				User.referenceToStaticMethod();
			}
		};
		job.action();
		
		Job jobWithReferenceMethodLambda = () -> User.referenceToStaticMethod();
		jobWithReferenceMethodLambda.action();
		
		Job jobWithReferenceMethod = User::referenceToStaticMethod;
		jobWithReferenceMethod.action();
		
		/* Referencia a un metodo de instancia particular */
		User user = new User("Roo with lambda at reference method");
		Job jobParticularInstanceLambda = () -> user.referenceToParticularMethod();
		jobParticularInstanceLambda.action();
		user.setName("Roo at reference method");
		Job jobParticularInstance = user::referenceToParticularMethod;
		jobParticularInstance.action();
		
		/* Referencia a un metodo de instancia de un objeto arbitrario de un tipo particular */
		
		JobString jobStringLambda = (word) -> word.toUpperCase();
		System.out.println(jobStringLambda.action("Roo in lambda"));
		JobString jobString = String::toUpperCase;
		System.out.println(jobString.action("Roo in lambda with reference method"));
		
		List<User> users = new ArrayList<User>();
		
		users.add(new User("Roo"));
		users.add(new User("Bel"));
		users.add(new User("Mar"));
		users.add(new User("Mic"));
		users.add(new User("Alej"));
		
		users.forEach(u -> u.showName());
		System.out.println("---------------------");
		users.forEach(User::showName);
		
		/* Referencia a un constructor */
		
		IUser user1 = new IUser() {
			@Override
			public User create(String name) {
				return new User(name);
			}
		};
		
		IUser user2 = (name -> new User(name));
		IUser user3 = User::new;
		user1.create("Roo1");
		user2.create("Roo2");
		user3.create("Roo3");
	}

}
