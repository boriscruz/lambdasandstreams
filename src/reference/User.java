/**
 * 
 */
package reference;

/**
 * @author boris
 *
 */
public class User {

	private String name;

	public User(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static void referenceToStaticMethod() {
		System.out.println("Testing reference to Static Method");
	}

	public void referenceToParticularMethod() {
		System.out.println("Testing to Particular Object Method");
	}

	public void showName() {
		System.out.println(name);
	}

}
