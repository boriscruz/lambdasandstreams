/**
 * 
 */
package optional;

import java.util.Optional;

/**
 * @author boris
 *
 */
public class OptionalMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		orElseOptional(null);
		orElseThrow(null);
	}
	
	public static void orElseOptional(String name) {
		Optional<String> optional = Optional.ofNullable(name);
		String nameOfNullable = optional.orElse("Roo is empty");
		System.out.println(nameOfNullable);
	}

	public static void orElseThrow(String name) {
		Optional<String> optional = Optional.ofNullable(name);
		optional.orElseThrow(NullPointerException::new);
		String nameNullable = optional.get();
		System.out.println(nameNullable);
	}
	
}
