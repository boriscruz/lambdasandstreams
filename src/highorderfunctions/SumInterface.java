package highorderfunctions;

public interface SumInterface {

	public int apply(int a, int b);

}
