/**
 * 
 */
package highorderfunctions;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author boris
 *
 */
public class HighOrderFunctions implements SumInterface {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		HighOrderFunctions hof = new HighOrderFunctions();

		System.out.println(hof.sum(3, 3));
		System.out.println(hof.apply(4, 4));

		SumInterface sum = (a, b) -> a + b;
		System.out.println(hof.sumHighOrderFunctions(sum, 2, 5));

		Function<String, String> toUppercase = (name) -> name.toUpperCase();

		hof.toUppercase(toUppercase, "roo");

		List<Integer> numbers = Arrays.asList(1, -2, 4, 6, 12, -32, -12, 56, 22, 51, -44, -23, -1, 1, -10);

		BiFunction<List<Integer>, Predicate<Integer>, List<Integer>> filter = (list, predicate) -> list.stream()
				.filter(e -> predicate.test(e)).collect(Collectors.toList());

		System.out.println(filter.apply(numbers, e -> e > 0));

	}

	public int sum(int a, int b) {
		return a + b;
	}

	@Override
	public int apply(int a, int b) {
		return a + b;
	}

	public int sumHighOrderFunctions(SumInterface sumInterface, int a, int b) {
		return sumInterface.apply(a, b);
	}

	public void toUppercase(Function<String, String> toUppercaseFunction, String name) {
		System.out.println(toUppercaseFunction.apply(name));
	}

}
